import { createStore } from 'vuex'
import until from './modules/until'
import userSession from './modules/user-session'
import album from './modules/album'
import track from './modules/track'
import player from './modules/player'

const store = createStore({
    modules: {
      until,
      album,
      userSession,
      track,
      player,
    },
})

export default store