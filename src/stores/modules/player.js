import { fetcherTimeout } from '../../utils/fetcher'

export default {
  namespaced: true,
  state: {
    image: null,
    isPlay: false,
    song: null,
    title: 'Unknow',
    author: 'Unknow',
    pkTrack: null,
  },
  mutations: {
    setImage (state, data) {
      state.image = data
    },
    setIsPlay (state, data) {
      state.isPlay = data
    },
    setSong (state, data) {
      state.song = data
    },
    setAuthor (state, data) {
      state.author = data
    },
    setTitle (state, data) {
      state.title = data
    },
    setPkTrack (state, data) {
      state.pkTrack = data
    }
  },
  getters:{
  },
  actions:{
    play ({ commit }, credentials) {
      commit('setSong', credentials.song)
      commit('setImage', credentials.image)
      commit('setIsPlay', credentials.isPlay)
      commit('setAuthor', credentials.author)
      commit('setTitle', credentials.title)
      commit('setPkTrack', credentials.pkTrack)
    },
  },
}