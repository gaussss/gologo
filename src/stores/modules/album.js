import { fetcherTimeout } from '../../utils/fetcher'

import { useStore } from 'vuex'
const store = useStore()

export default {
  namespaced: true,
  state: {
    albumsList: null,
  },
  mutations: {
    setAlbumsList (state, data) {
      state.albumsList = data
    }
  },
  getters:{
    getAlbumsList: (state) => state.albumsList
  },
  actions:{
    getSelfAlbums ({ commit, rootState }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/album/', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key,
          }
        })
      )
    },
    getAlbums ({ commit, rootState }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/album/?style=' + rootState.until.selectedStyle, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key,
          }
        })
      )
    },
    discover ({ commit, rootState }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/discover/?style=' + rootState.until.selectedStyle, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    getRecommanded({ commit }, credentials) {
      return fetcherTimeout(
        60000,
        fetch('/api/stock/rec/', {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    searchTracks ({ commit, rootState }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/search/?style=' + rootState.until.selectedStyle, {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    getTracks ({ commit, rootState }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/track/?style=' + rootState.until.selectedStyle, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    top50 ({ commit, rootState }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/top50/?style=' + rootState.until.selectedStyle, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    top50album ({ commit, rootState }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/top50album/?style=' + rootState.until.selectedStyle, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    create ({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/album/', {
          method: 'POST',
          body: JSON.stringify(credentials.body),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    update ({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/album/'+credentials.pk+'/', {
          method: 'PUT',
          body: JSON.stringify(credentials.body),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    }
  },
}