import { fetcherTimeout } from '../../utils/fetcher'

export default {
  namespaced: true,
  state: {
    user_data: {
      detail: [
        { "id": 0, 
          "image": "", 
          "phone": "", 
          "genre": "", 
          "state": "", 
          "city": "", 
          "birth_day": "", 
          "is_artist": true, 
          "is_subscriber": false, 
          "created_at": "", 
          "updated_at": "", 
          "user": 0, 
          "favory": [] }
        ],
      username: "",
      pk: 0
    },
    api_key: null,
    role: null
  },
  mutations: {
    setApiKey(state, data) {
      state.api_key = data
    },
    setUserData(state, data) {
      state.user_data = data
    },
    setRole(state, data) {
      state.role = data
    }
  },
  getters: {
    user_data: (state) => state.user_data
  },
  actions: {
    authentificate({ commit }) {
      const dateNow = Date.now()
      const exp = sessionStorage.getItem('exp')

      if (sessionStorage.getItem('key') == 'null' || sessionStorage.getItem('key') == null || sessionStorage.getItem('key')==undefined || sessionStorage.getItem('key')=='undefined') {
        return false
      }
      if (exp == null) {
        return false
      }
      if (dateNow >= exp) {
        return false
      }
      commit('setApiKey', sessionStorage.getItem('key'))
      return true
    },
    getThisUser({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/core/user/' + credentials.pk + '/', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    getAtivity({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/history/?limit=50&user=' + credentials.pk, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    crack({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/history/', {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    login({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/core/token/', {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
          }
        })
      )
    },
    logout({ commit }) {
      sessionStorage.setItem('key', null)
      commit('setApiKey', null)
      commit('setUserData', {
        detail: [
          { "id": 0, 
            "image": "", 
            "phone": "", 
            "genre": "", 
            "state": "", 
            "city": "", 
            "birth_day": "", 
            "is_artist": true, 
            "is_subscriber": false, 
            "created_at": "", 
            "updated_at": "", 
            "user": 0, 
            "favory": [] }
          ],
        username: "",
        pk: 0
      })
      return true
    },
    register({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/core/register/', {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
          }
        })
      )
    },
    update({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/core/update/', {
          method: 'PUT',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
  }
}
