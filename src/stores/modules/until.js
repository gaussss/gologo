import { fetcherTimeout } from '../../utils/fetcher'

export default {
  state: {
      isH: false,
      proxy: '/api',
      navLeft: true,
      showNameApp: true,
      categories: [],
      selectedStyle: -1
  },
  mutations: {
    setIsH (state, data) {
      state.isH = data
    },
    setshowNameApp (state, data) {
      state.showNameApp = data
    },
    setCategories (state, data) {
      state.categories = data
    },
    setSelectedStyle (state, data) {
      state.selectedStyle = data
    }
  },
  actions: {
    getCategories ({ commit }, credentials) {
      return fetcherTimeout(
        10000,
        fetch('/api/stock/category/', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    }
  }
}