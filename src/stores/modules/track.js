import { fetcherTimeout } from '../../utils/fetcher'

export default {
  namespaced: true,
  state: {
    albumsList: null,
    similarsTrack: []
  },
  mutations: {
    setAlbumsList (state, data) {
      state.albumsList = data
    },
    setSimilasTrackList (state, data) {
      state.similarsTrack = data
    }
  },
  getters:{
    getAlbumsList: (state) => state.albumsList
  },
  actions:{
    favorite ({ commit }, credentials) {
      return fetcherTimeout(
        20000,
        fetch('/api/stock/track/'+credentials.user+'/get_favorite/', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    similars ({ commit }, credentials) {
      return fetcherTimeout(
        20000,
        fetch('/api/stock/similar/', {
          method: 'POST',
          body: JSON.stringify(credentials.body),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    makeFavorite ({ commit }, credentials) {
      return fetcherTimeout(
        20000,
        fetch('/api/stock/track/'+credentials.track_id+'/make_favorite/', {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    removeFavorite ({ commit }, credentials) {
      return fetcherTimeout(
        20000,
        fetch('/api/stock/track/'+credentials.track_id+'/remove_favorite/', {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    create ({ commit }, credentials) {
      return fetcherTimeout(
        20000,
        fetch('/api/stock/track/', {
          method: 'POST',
          body: JSON.stringify(credentials.body),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    },
    update ({ commit }, credentials) {
      return fetcherTimeout(
        20000,
        fetch('/api/stock/track/'+credentials.pk+'/', {
          method: 'PUT',
          body: JSON.stringify(credentials.body),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: 'Bearer ' + credentials.api_key
          }
        })
      )
    }
  },
}