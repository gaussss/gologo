import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import Home from '../views/Home/Main.vue'
import Layout from '../layout/Main.vue'
import Album from '../views/Album/Main.vue'
import User from '../views/User/Main.vue'
import Catalog from '../views/Catalog/Main.vue'
import Search from '../views/Search/Main.vue'
import Albums from '../views/Channel/Albums.vue'
import Tracks from '../views/Channel/Tracks.vue'
import Edition from '../views/Channel/Main.vue'
import Artist from '../views/Channel/Artist.vue'
import TracksFront from '../views/Album/Tracks.vue'
import H from '../views/H/H.vue'
import Login from '../views/Authentification/Login.vue'
import Profil from '../views/Profil/Main.vue'
import Setting from '../views/Profil/Setting.vue'
import PlayList from '../views/PlayList/Main.vue'

import LayoutEdit from '../layout/editLayout/Main.vue'
import ErrorPage from '../views/error-page/Main.vue'

const routes = [
	{
		path: '/edit',
		name: 'Edit',
		component: LayoutEdit,
		children: [
			{
				path: '/playlist',
				name: 'playlist',
				component: PlayList,
			},
		]
	},
	{
		path: '/',
		name: 'Global',
		component: Layout,
		children: [
			{
				path: '/playlist',
				name: 'playlist',
				component: PlayList,
			},
			{
				path: '/:pk/setting',
				name: 'setting',
				component: Setting,
				props: true
			},
			{
				path: 'profil',
				name: 'profil',
				component: Profil
			},
			{
				path: 'home',
				name: 'h',
				component: H
			},
			{
				path: '',
				name: 'Home',
				component: Home
			},
			{
				path: 'users',
				name: 'User',
				component: User
			},
			{
				path: 'albums',
				name: 'Album',
				component: Album
			},
			{
				path: '/:name/:pk/track',
				name: 'TracksFront',
				component: TracksFront,
				props: true
			},
			{
				path: 'catalog/:title/:functionn/:menu',
				name: 'Catalog',
				component: Catalog,
				props: true
			},
			{
				path: 'search',
				name: 'Search',
				component: Search
			},
			{
				path: 'channel/edition/',
				name: 'Edition',
				component: Edition,
				children: [
					{
						path: 'artist',
						name: 'Artist',
						component: Artist
					},
				]
			},
			{
				path: 'channel/albums',
				name: 'Channel',
				component: Albums
			},
			{
				path: 'channel/albums/:albumId/tracks',
				name: 'Tracks',
				component: Tracks,
				props: true
			},
		]
	},
	{
		path: '/login',
		name: 'login',
		component: Login,
	},
	{
    path: '/error-page',
    name: 'error-page',
    component: ErrorPage
  },
  {
    path: '/:pathMatch(.*)*',
    component: ErrorPage
  }
]
const router = createRouter({
	history: createWebHistory(),
	routes,
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || { left: 0, top: 0 }
  }
})
export default router