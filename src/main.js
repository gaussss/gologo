import { createApp } from 'vue'
import store from './stores/index'
import App from './App.vue'
import './index.css'
import router from "./router/index"

createApp(App).use(store).use(router).mount('#app')
