function getTitle (vm) {
  const {
    title
  } = vm.$options
  if (title) {
    return typeof title === 'function' ? title.call(vm) : title
  }
}
export default {
  created () {
    const title = getTitle(this)
    if (title) {
      document.title = title
    }
  }
}

export function fetcherTimeout (ms, promise) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      reject(new Error('timeout'))
    }, ms)
    promise.then(resolve, reject)
    promise.then(function (response) {
      console.log(response)
    })
  })
}
