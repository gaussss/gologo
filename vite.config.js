import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
const path = require("path");

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    host: true,
    proxy: {
      // string shorthand
      "/api": {
        // ws: true,
        target: "http://localhost:8000",
        changeOrigin: false,
        secure: false,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    }
  }
})
