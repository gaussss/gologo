const colors = require('tailwindcss/colors')

module.exports = {
    content: [
      "./index.html",
      "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    darkMode: 'class',
    theme: {
      screens: {
        '2xm': '320px',
        'xm': '350px',
        'sm': '640px',
        'md': '768px',
        'lg': '1024px',
        'xl': '1280px',
        '2xl': '1536px',
      },
      extend: {
        fontFamily: "Roboto"
      },
    },
    plugins: [],
  }